<?php
return [
	'cxuu_cache_mod' => 0,//自定义非框架提供的 全局 setCache($ckey, $data,$timeout) 缓存数据模式，支持任意数据，1:redis, 2:memcached, 默认：0 文件
	'DEBUG' => [
		'level' => 3,//debug级别：0:关闭，1：只显示运行信息，2：显示运行和错误信息，3：显示运行、错误、警告信息
		'log' => 1,//日志级别：0:不记录，1:只记录错误，2：记录错误和警告
		'type' => 'json',//输出格式	auto, html，json	默认：auto
	],
	'ROUTER' => [
        'mod' => 0, //路由模式：0:queryString，1：pathInfo，2：路由
        'module' => false,//是否启用模块
        'restfull' => null,//有此字段且不为null 则启用restfull模式，例如将POST操作映射到某个控制器的add方法	restfull接口需要使用pathinfo或者路由模式，可以配合 rewrite 伪静态
    ],
	'POWEREDBY'=>'cde',
	'DEBUG_MSG'=>'html',//debug信息的输出方式	html，json
	'HEADER_VER'=>'',	
	'SESSION' => [
		'name' => 'SID', //更改 session 名  可解决 同域名不同端口下Session冲突问题
		'auto' => true, //自动开启 session
		'httponly'=>true,
		'redis' => false,
		'host' => '',
		'port' => '',
		'pass' => ''
	],
	'VIEW' => [
		'php_tag' => 'php',
		'import_tag' => 'import',
		'template_tag' => 'template',
		'ext' => '.html',
		'theme' => 'default',
		'prefix' => '<{',
		'suffix' => '}>',
		'compress' => false
	],
	'DB' => [
		'dsn' => 'mysql:host=127.0.0.1;dbname=cxuuweb_v3;port=3306',
		'db' => 'cxuuweb_v3',
		'user' => 'root',
		'pass' => 'cde',
		'charset' => 'utf8',
		'prefix' => 'cxuu_',
		'cache_mod' => 0, //查询数据库缓存模式：1:redis, 2:memcached, 默认：0 文件
	],
	'REDIS' => [
		'host' => '127.0.0.1',
		'port' => '6379',
		'pass' => '',
		'db' => 1,
		'timeout' => 1,
	],
];
