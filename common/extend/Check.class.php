<?php
//表单验证类  20200515 组装
namespace common\extend;

class Check
{
	/*
		用法：
		Check::rule(
					array(验证函数1，'错误返回值1'),
					array(验证函数2，'错误返回值2'),
					);
		若有一个验证函数返回false,则返回对应的错误返回值，若全部通过验证，则返回true。
		验证函数，可以是自定义的函数或类方法，返回true表示通过，返回false，表示没有通过

		使用样例
        $str='127.1.1.122';
        $str1='127.1.1.1212';
        $str2='127.1.1.1212';
        $msg=Check::rule(
                    array(check::must($str),'必须的'),
                    array(check::tel('020-12345678'),'固定电话号码格式不对'),
                    array(check::len($str,6,10),'长度不对'),
                    array(check::same($str1,$str2),'不相同'),
                    array(check::email('404352772@qq.com'),'邮箱不对')
                    );
        if($msg!==true)
        {
            echo 'error:'.$msg;
        }
        else
        {
            echo 'ok';
		}
		
	*/
	public static function rule($array = array())
	{
		//可以采用数组传参，也可以采用无限个参数方式传参
		if (!isset($array[0][0]))
			$array = func_get_args();

		if (is_array($array)) {
			foreach ($array as $vo) {
				if (is_array($vo) && isset($vo[0]) && isset($vo[1])) {
					if (!$vo[0])
						return $vo[1];
				}
			}
		}
		return true;
	}

	//检查字符串长度
	public static function len($str, $min = 0, $max = 255)
	{
		$str = trim($str);
		if (empty($str))
			return true;
		$len = strlen($str);
		if (($len >= $min) && ($len <= $max))
			return true;
		else
			return false;
	}
	//检查字符串是否为空
	public static function must($str)
	{
		$str = trim($str);
		return empty($str) ? false : true;
	}

	//检查两次输入的值是否相同
	public static function same($str1, $str2)
	{
		return $str1 == $str2;
	}
	//检查用户名
	public static function userName($str, $len_min = 0, $len_max = 255, $type = 'ALL')
	{
		if (empty($str))
			return true;
		if (self::len($str, $len_min, $len_max) == false) {
			return false;
		}

		switch ($type) {				//纯英文
			case "EN":
				$pattern = "/^[a-zA-Z]+$/";
				break;
				//英文数字                           
			case "ENNUM":
				$pattern = "/^[a-zA-Z0-9]+$/";
				break;
				//允许的符号(|-_字母数字)   
			case "ALL":
				$pattern = "/^[\-\_a-zA-Z0-9]+$/";
				break;
				//用户自定义正则
			default:
				$pattern = $type;
				break;
		}

		if (preg_match($pattern, $str))
			return true;
		else
			return false;
	}

	//验证邮箱
	public static function email($str)
	{
		if (empty($str))
			return true;

		return	filter_var($str, FILTER_VALIDATE_EMAIL);

		/* $chars = "/^([a-z0-9+_]|\\-|\\.)+@(([a-z0-9_]|\\-)+\\.)+[a-z]{2,6}\$/i";
		if (strpos($str, '@') !== false && strpos($str, '.') !== false){
			if (preg_match($chars, $str)){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		} */
	}
	//验证手机号码
	public  static function mobile($str)
	{
		if (empty($str)) {
			return true;
		}

		return preg_match('#^13[\d]{9}$|14^[0-9]\d{8}|^15[0-9]\d{8}$|^18[0-9]\d{8}$#', $str);
	}
	//验证固定电话
	public  static function tel($str)
	{
		if (empty($str)) {
			return true;
		}
		return preg_match('/^((\(\d{2,3}\))|(\d{3}\-))?(\(0\d{2,3}\)|0\d{2,3}-)?[1-9]\d{6,7}(\-\d{1,4})?$/', trim($str));
	}
	//验证qq号码
	public  static function qq($str)
	{
		if (empty($str)) {
			return true;
		}

		return preg_match('/^[1-9]\d{4,12}$/', trim($str));
	}
	//验证邮政编码
	public  static function zipCode($str)
	{
		if (empty($str)) {
			return true;
		}

		return preg_match('/^[1-9]\d{5}$/', trim($str));
	}
	//验证ip
	public static function ip($str)
	{
		if (empty($str))
			return true;
		return filter_var($str, FILTER_VALIDATE_IP);
	}
	//验证身份证(中国)
	public  static function idCard($str)
	{
		$str = trim($str);
		if (empty($str))
			return true;

		if (preg_match("/^([0-9]{15}|[0-9]{17}[0-9a-z])$/i", $str))
			return true;
		else
			return false;
	}

	//验证网址
	public  static function url($str)
	{
		if (empty($str))
			return true;

		//return preg_match("/^(http|https|ftp|ftps):\/\/[AZaz09]+\.[AZaz09]+[\/=\?%\&_~`@[\]\':+!]*([^<>\"\"])*$/", $str) ? true : false;
		return filter_var($str, FILTER_VALIDATE_URL);
	}

	/**
	 * 匹配日期
	 * @param string $value
	 */
	public static function checkDate($str)
	{
		$dateArr = explode("-", $str);
		if (is_numeric($dateArr[0]) && is_numeric($dateArr[1]) && is_numeric($dateArr[2])) {
			if (($dateArr[0] >= 1000 && $dateArr[0] <= 10000) && ($dateArr[1] >= 0 && $dateArr[1] <= 12) && ($dateArr[2] >= 0 && $dateArr[2] <= 31))
				return true;
			else
				return false;
		}
		return false;
	}
	/**
	 * 匹配时间
	 * @param string $value
	 */
	public static function checkTime($str)
	{
		$timeArr = explode(":", $str);
		if (is_numeric($timeArr[0]) && is_numeric($timeArr[1]) && is_numeric($timeArr[2])) {
			if (($timeArr[0] >= 0 && $timeArr[0] <= 23) && ($timeArr[1] >= 0 && $timeArr[1] <= 59) && ($timeArr[2] >= 0 && $timeArr[2] <= 59))
				return true;
			else
				return false;
		}
		return false;
	}

	/**
	 * 验证密码
	 * @param string $value
	 * @param int $minLen  $maxLen
	 * @return boolean
	 */
	public static function isPWD($value, $minLen = 6, $maxLen = 16)
	{
		if (empty($value))
			return true;
		return preg_match('/^(\w*(?=\w*\d)(?=\w*[A-Za-z])\w*){' . $minLen . ',' . $maxLen . '}$/', $value) ? true : false;
	}
	/**
	 * 验证中文
	 * @param:string $str 要匹配的字符串
	 * @param:$charset 编码（默认utf-8,支持gb2312）
	 */
	public static function isChinese($str)
	{
		if (empty($str))
			return true;
		return preg_match('/^[\x7f-\xff]+$/', $str) ? true : false;
	}

	/**
	 * 数字验证
	 * param:$flag : int是否是整数，float是否是浮点型
	 */
	public static function isNum($str, $flag = 'float')
	{
		if (empty($str))
			return true;
		if (strtolower($flag) == 'int') {
			return ((string)(int)$str === (string)$str) ? true : false;
		} else {
			return ((string)(float)$str === (string)$str) ? true : false;
		}
	}
	/**
	 * 图片地址验证
	 */
	public static function isImg($str)
	{
		if (empty($str))
			return true;
		return preg_match('/.*(\.png|\.jpg|\.jpeg|\.gif)$/', $str) ? true : false;
	}
}
