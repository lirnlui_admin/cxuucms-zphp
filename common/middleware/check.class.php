<?php

namespace common\middleware;

class check
{
	//检查是否登录
	public static function isLogin()
	{
		if (!isset($_SESSION["cxuu_admin"]) && !strpos(ROUTE['ctrl'], 'login')) {
			redirect('/admin?c=login');
		}
	}

	//判断权限
	public static function auth()
	{
		$controler = ROUTE['ctrl'];
		$action = ROUTE['act'];
		if (sessionInfo('gid') != 1) {
			$basepurview = unserialize(sessionInfo('systemrole'));
			$contrAndAction = strtolower($controler . "_" . $action); //转换成小写
			$basepurview = array_map('strtolower', $basepurview); //转换成小写
			//P($basepurview);
			if (!in_array($contrAndAction, $basepurview)) {
				//echo "no";
				//echo "<script> layer.msg('你无权限操作此功能！', {icon: 2, anim: 6, time: 2000});</script>";
				json(array('status' => 0, 'info' => '你无权限操作此功能！'));
				exit;
			}
		}
	}
}
