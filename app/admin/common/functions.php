<?php
/*
* 龙啸轩内容管理系统 BY Cdeng in Lhasa “不会画画的编程爱好者不是一个好木匠”
*/
const CXUUCMS_V = "cxuucms V3.3.1 20210630";

/* 
 *  获取网站系统配置信息方法
 *  使用方法：在模板<?php echo siteInfo('copyright');?> 
 */
function siteInfo($key)
{
	return \model\siteconfig::getSiteInfo($key);
}

/**
 * 写入数据库操作日志
 * insertSqlLog($info);
 * **/
function insertSqlLog(string $info)
{
	$log_sql = new \model\log_sql();
	$log_sql->insertData($info);
}
/* 
 *  加载后台语言包
 *  使用方法：在模板<?php echo lang('home');?>   输出：后台首页
 */
function lang($val, $l = 'zh-cn')
{
	$lang = root\base\ex_cache::getCache('langCache');
	if (empty($lang)) {
		$lang = include_once(P_APP . "/base/lang/" . $l . ".class.php");
		root\base\ex_cache::setCache('langCache', $lang, 526410);
	}
	return $lang[$val];
}
/* 
 *  生成后台常用路径 生成 admin.php?c=member&a=del  后台部分未使用路由模式，使用这种方式较好
 *  使用方法：在模板<?php echo caUrl('member','del');?> 
 */
function caUrl($c, $a)
{
	return PHP_FILE . '?c=' . $c . '&a=' . $a;
}

/* 
 *  获取 $_SESSION 值
 *  参照login.class.php 的登录方法调用相应数据
 *  使用方法：在模板<?php echo sessionInfo("nickname");?> 
 */
function sessionInfo($val)
{
	return $_SESSION['cxuu_admin'][$val];
	//$cookie = $_COOKIE['cxuu_admin'];
	//return $cookie[$val];
}

/* 
 *  对比数组是否有指定值
 *  返回 checkbox 选定状态
 *  使用方法：用于系统权限设置页<?php echo sysRoC("siteconfig_index",$systemrole);?>
 */
function sysRoC($val, $arr)
{
	if (is_array($arr))
		return in_array($val, $arr) ? 'checked' : '';
}

/* 
 *  获取系统拥有的栏目权限
 */
function channlrole()
{
	return implode(",", unserialize(sessionInfo("channlrole")));
}
/* 
 *  获取系统拥有的后台菜单权限
 */
function adminMenuRole()
{
	return implode(",", unserialize(sessionInfo("menurole")));
}
/* 
 *  判断是否拥有的系统功能权限
 $val 为权限值，如： articlecontent_addview
 */
function sysrolTF($val)
{
	if (sessionInfo("gid") != 1)
		return in_array(strtolower($val), array_map('strtolower', unserialize(sessionInfo("systemrole")))) ? true : false;
	else
		return true;
}


/* 
 *  拓展Redis 缓存清除方法
 *  =================================
 *	$key 删除指定ID 否则清空全部
 *  
 */
function RCD($key = null)
{
	if ($GLOBALS['ZPHP_CONFIG']['DB']['cache_mod'] == 1) {
		$c = z\cache::Redis();
		if (null !== $key)
			$result = $c->delete($key);
		else
			$result = $c->flushAll();
		return $result;
	} else {
		return 0;
	}
}

/* 
 *  拓展Redis 获取全部key 计算有多少条 缓存数据
 *  =================================
 *	$c->keys('*') 全部KEY
 *  
 */
function RCA()
{
	if ($GLOBALS['ZPHP_CONFIG']['DB']['cache_mod'] == 1) {
		$c = z\cache::Redis();
		$result = $c->keys('*');
		$count = count($result);
		return $count;
	} else {
		return 0;
	}
}

/* 
 *  后台系统菜单
 *  =================================
 *	引用方式实现无限极分类
 *  使用方法：generateTree($data)
 */
function generateTree($data)
{
	$items = array();
	foreach ($data as $v) {
		$items[$v['id']] = $v;
	}
	$tree = array();
	foreach ($items as $k => $item) {
		if (isset($items[$item['pid']])) {
			$items[$item['pid']]['haveChild'] = true;
			$items[$item['pid']]['href'] = true;
			$items[$item['pid']]['spread'] = "true";
			$items[$item['pid']]['children'][] = &$items[$k];
		} else {
			$tree[] = &$items[$k];
		}
	}
	return $tree;
}


/* 
 *  后台 内容管理 栏目分类菜单
 *  =================================
 *	引用方式实现无限极分类
 *  使用方法：contentCateTree($data)
 */
function contentCateTree($data)
{
	$dataRename = array();
	foreach ($data as $key => $newdata) {
		$dataRename[$key]['id'] = $data[$key]['id'];
		$dataRename[$key]['parentId'] = $data[$key]['pid'];
		$dataRename[$key]['title'] = $data[$key]['name'];
		$dataRename[$key]['type'] = $data[$key]['type'];

		if ($dataRename[$key]['type'] == 0) {
			$dataRename[$key]['disabled'] = "true";
			$dataRename[$key]['spread'] = "true";
		}
	}

	$items = array();
	foreach ($dataRename as $v) {
		$items[$v['id']] = $v;
	}
	$tree = array();
	foreach ($items as $k => $item) {
		if (isset($items[$item['parentId']]))
			$items[$item['parentId']]['children'][] = &$items[$k];
		else
			$tree[] = &$items[$k];
	}
	return $tree;
}

/* 
 *  递归方式实现无限极分类
 *  使用方法：getTree($data)
 */
function getTree($arr, $pid = 0, $level = 0)
{
	static $list = [];
	foreach ($arr as $key => $value) {
		if ($value["pid"] == $pid) {
			$value["level"] = $level;
			$value["open"] = true;
			$list[] = $value;
			unset($arr[$key]); //删除已经排好的数据为了减少遍历的次数
			getTree($arr, $value["id"], $level + 1);
		}
	}
	return $list;
}

/** 
 * 统计目录文件大小
 */
function dirSize($file)
{
	$size = 0;
	if (is_dir($file)) {
		$dir = opendir($file);
		while (false !== ($filename = readdir($dir))) {
			if ($filename != "." && $filename != "..") {
				$filename = $file . "/" . $filename;
				if (is_dir($filename))
					$size += dirsize($filename);
				else
					$size += filesize($filename);
			}
		}
		closedir($dir);
		return $size;
	} else {
		return 0;
	}
}

/** 
 * 统计目录文件大小
 */
$fileNum = 0;
$dirNum = 0;
function dirFileNum($file)
{
	global $fileNum;
	global $dirNum;
	//count(glob($file."*"));
	if (is_dir($file)) {
		$dir = opendir($file);
		while (false !== ($filename = readdir($dir))) {
			if ($filename != "." && $filename != "..") {
				$filename = $file . "/" . $filename;
				if (is_dir($filename)) {
					$dirNum += 1;
					dirFileNum($filename);
				} else {
					$fileNum += 1;
				}
			}
		}
		closedir($dir);
		$num = [];
		$num['fileNum'] = $fileNum;
		$num['dirNum'] = $dirNum;
		return $num;
	} else {
		return 0;
	}
}


/**
 * 删除目录及文件
 * @param  [type]  $dirName [description]
 * @param  boolean $t       [是否删除目录]
 * @param  integer $i       [description]
 * @return integer          [删除的文件数]
 */
function del_dir($dirName, $t = true, $i = 0)
{
	if ($handle = opendir($dirName)) {
		while (false !== ($item = readdir($handle))) {
			if ($item != "." && $item != "..") {
				if (is_dir($dirName . '/' . $item)) {
					$ii = del_dir($dirName . '/' . $item, $t);
					$i += $ii;
				} elseif (unlink($dirName . '/' . $item)) $i++;
			}
		}
		closedir($handle);
	} else return false;
	$t && rmdir($dirName);
	return $i;
}

/**
 * 删除文件
 * @param  [type]url string  $path 主要为附件地址 如：/uploads/attment/202005/10/5eb79b20964504346290.txt
 * del_file('/uploads/attment/202005/10/5eb7830069d7e2556290.txt');
 */

function del_file(string $path)
{
	$path = P_PUBLIC . $path;
	$url = iconv('utf-8', 'gbk', $path);
	if (PATH_SEPARATOR == ':')  //linux
		unlink($path);
	else
		unlink($url);
}
