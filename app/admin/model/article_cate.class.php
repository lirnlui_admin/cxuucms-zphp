<?php

namespace model;

use root\base\model;

class article_cate extends model
{
	public function FindData()
	{
		$db = $this->db();
		$where['id'] = $_GET['id'];
		return $db->table('article_cate')->where($where)->find();
	}

	//根据栏目权限获取栏目列表  设置$auth = 1  走权限，否则查询全部数据
	static public function SelectData($auth=null)
	{
		$pdo = \z\pdo::Init();
		$prefix = $pdo->GetConfig(); //获取PDO设置
		$fix = $prefix['prefix'];
		if ($auth == 1) {
			$channlid = channlrole();
			if (sessionInfo('gid') != 1) {
				$sql = "SELECT * FROM `{$fix}article_cate` WHERE `id` IN ({$channlid}) ORDER BY sort asc";
			} else {
				$sql = "SELECT * FROM `{$fix}article_cate` ORDER BY sort asc";
			}
		} else {
			$sql = "SELECT * FROM `{$fix}article_cate` ORDER BY sort asc";
		}
		return $pdo->QueryAll($sql);
	}

	//根据栏目权限获取栏目树列表
	public function SelectTreeData()
	{
		$cate = new \common\extend\Category(array('id', 'pid', 'name', 'cname'));
		return $cate->getTree(self::SelectData(1));
	}

	//添加数据
	public function insertData()
	{
		if (empty($_POST['name']))
			return ['status' => 0, 'msg' => '请填写必要数据'];
		$db = $this->db('article_cate');
		$data = [
			'pid' => $_POST['pid'],
			'name' => varFilter($_POST['name']),
			'type' => $_POST['type'],
			'theme' => varFilter($_POST['theme'])??null,
			'sort' => $_POST['sort']?:1,
			'num' => $_POST['num']?:25,
			'color' => varFilter($_POST['color']),
			'ct' => $_POST['ct'],
			'urlname' => varFilter($_POST['urlname'])??null,
		];
		$re_key = $db->insert($data);
		if ($re_key) {
			//写入日志
			insertSqlLog("添加栏目 ID：" . $re_key);
			//更新缓存
			$fix = $db->GetPrefix();
			$db->CleanCache($GLOBALS['ZPHP_CONFIG']['DB']['db'], $fix . "article_cate");

			return ['key' => $re_key, 'status' => 1, 'msg' => '添加成功'];
		} else {
			return ['status' => 0, 'msg' => '数据错误！添加失败'];
		}
	}

	//更新数据
	public function updateData()
	{
		if (empty($_POST['name']))
			return ['status' => 0, 'msg' => '请填写必要数据'];

		if ($_POST['pid'] == $_POST['id'])
			return ['status' => 0, 'msg' => '栏目不能选择自己作为父级'];

		$db = $this->db();
		$where['id'] = $_POST['id'];
		$data = [
			'pid' => $_POST['pid'],
			'name' => varFilter($_POST['name']),
			'type' => $_POST['type'],
			'theme' => varFilter($_POST['theme'])??null,
			'sort' => $_POST['sort']?:1,
			'num' => $_POST['num']?:25,
			'color' => varFilter($_POST['color']),
			'ct' => $_POST['ct'],
			'urlname' => varFilter($_POST['urlname'])??null,
		];
		$re_key = $db->table('article_cate')->Where($where)->Update($data);
		if ($re_key) {
			//写入日志
			insertSqlLog("修改栏目 ID：" . $_POST['id']);

			//更新缓存
			$fix = $db->GetPrefix();
			$db->CleanCache($GLOBALS['ZPHP_CONFIG']['DB']['db'], $fix . "article_cate");

			return ['status' => 1, 'msg' => '修改成功'];
		} else {
			return ['status' => 0, 'msg' => '数据没有变化'];
		}
	}

	//删除一条数据
	public function deleteOneData()
	{
		$db = $this->db();

		$where['cid'] = $_POST['id'];
		$find = $db->table('article')->where($where)->find();
		if ($find) {
			return false; //如该栏目下有内容，拒绝删除
		} else {
			$whereCate['id'] = $_POST['id'];
			insertSqlLog("删除栏目：" . $_POST['id']);//写入日志
			return $db->table('article_cate')->where($whereCate)->Delete();
		}
	}
}
