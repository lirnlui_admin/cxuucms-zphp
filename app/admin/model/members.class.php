<?php

namespace model;

use root\base\model;

class members extends model
{

	//查找一条信息
	public function findData()
	{
		$db = $this->db();
		$where['id'] = $_GET['id'];
		return $db->table('member')->where($where)->find();
	}

	//添加数据
	public function insertData()
	{
		if (empty($_POST['name']))
			return ['status' => 0, 'msg' => '请填写必要数据'];
		//插入数据信息部分
		$db = $this->db('member');
		$data = [
			'name' => varFilter($_POST['name']),
			'duties' => varFilter($_POST['duties']),
			'photo' => varFilter($_POST['photo']),
			'duty' => varFilter($_POST['duty']),
			'sort' => varFilter($_POST['sort']),
			'status' => $_POST['status'],
		];
		$re_key = $db->insert($data); //$this->error($db->getError());
		if ($re_key) {
			//写入日志
			insertSqlLog("插入成员功能信息 ID：" . $re_key);

			return ['key' => $re_key, 'status' => 1, 'msg' => '添加成功'];
		} else {
			return ['status' => 0, 'msg' => '数据错误！添加失败'];
		}
	}
	//关联更新数据
	public function updateData()
	{
		if (empty($_POST['name']))
			return ['status' => 0, 'msg' => '请填写必要数据'];
		$db = $this->db();
		$where['id'] = $_POST['id'];
		$data = [
			'name' => varFilter($_POST['name']),
			'duties' => varFilter($_POST['duties']),
			'photo' => varFilter($_POST['photo']),
			'duty' => varFilter($_POST['duty']),
			'sort' => varFilter($_POST['sort']),
			'status' => $_POST['status'],
		];

		$re_key = $db->table('member')->Where($where)->Update($data);
		if ($re_key) {
			//写入日志
			insertSqlLog("修改成员功能信息 ID：" . $_POST['id']);

			return ['status' => 1, 'msg' => '修改成功'];
		} else {
			return ['status' => 0, 'msg' => '数据没有变化'];
		}
	}


	public function switchStatus()
	{
		$db = $this->db();
		$where['id'] = $_POST['id'];
		$data = [
			'status' => $_POST['status']
		];
		insertSqlLog("切换成员显示状态 ID：" . $_POST['id']);//写入日志
		return $db->table('member')->where($where)->Update($data);
	}


	//删除一条数据
	public function deleteOneData()
	{
		$db = $this->db();
		$where['id'] = $_POST['id'];
		insertSqlLog("删除成员功能信息 ID：" . $_POST['id']);//写入日志
		return $db->table('member')->where($where)->Delete();
	}


	//获取数据并分页
	public function jsonPageSelect()
	{
		$db = $this->db();
		$p = intval($_GET['page'] ?? 0) ?: 1;
		$num = intval($_GET['limit'] ?? 0) ?: 15;
		$page = ['num' => $num, 'p' => $p, 'return' => true];
		$result['data'] = $db->table('member')->Page($page)->order('sort asc')->select();
		$result['page'] = $db->getPage();
		return $result;
	}
}
