<?php

namespace model;

use root\base\model;

class feedbacks extends model
{
    //查找一条数据
    public function findData()
    {
        $db = $this->db();
        $where['id'] = $_GET['id'];
        return $db->table('feedbacks')->where($where)->find();
    }

    //添加回复数据
    public function insertData()
    {
        if (empty($_POST['title']))
			return  ['status' => 0, 'msg' => '请填写必要数据'];
        $db = $this->db('feedbacks');
        $data = [
            'title' => varFilter($_POST['title']),
            'name' => varFilter($_POST['name']),
            'phone' => varFilter($_POST['phone']),
            'content' => varFilter($_POST['content']),
            'ip' => getip(),
            'mid' => $_POST['mid'],
            'time' => date('Y-m-d H:i:s', time()),
            'status' => $_POST['status'],
            /* 're_content' => $_POST['re_content'],
			're_status' => $_POST['re_status'],
			're_time' => date('Y-m-d H:i:s',time()), */
        ];
        $re_key = $db->insert($data);
        if ($re_key) {
            //写入日志
            insertSqlLog("添加意见反馈 ID：" . $re_key);

            return ['key' => $re_key, 'status' => 1, 'msg' => '添加成功'];
        } else {
            return ['status' => 0, 'msg' => '数据错误！添加失败'];
        }
    }
    //更新回复数据
    public function updateData()
    {
        if (empty($_POST['title']))
			return  ['status' => 0, 'msg' => '请填写必要数据'];
        $db = $this->db();
        $where['id'] = $_POST['id'];
        $data = [
            'title' => varFilter($_POST['title']),
            'name' => varFilter($_POST['name']),
            'phone' => varFilter($_POST['phone']),
            'content' => varFilter($_POST['content']),
            //'mid' => $_POST['mid'],
            //'time' => date('Y-m-d H:i:s',time()),
            'status' => $_POST['status'],
            're_content' => varFilter($_POST['re_content']),
            're_status' => $_POST['re_status'],
            're_time' => date('Y-m-d H:i:s', time()),
        ];
        $re_key = $db->table('feedbacks')->Where($where)->Update($data);
        if ($re_key) {
            //写入日志
            insertSqlLog("修改或回复意见反馈 ID：" . $_POST['id']);

            return ['status' => 1, 'msg' => '修改成功'];
        } else {
            return ['status' => 0, 'msg' => '数据没有变化'];
        }
    }

    //删除一条数据
    public function deleteOneData()
    {
        $db = $this->db();
        $where['id'] = $_POST['id'];
        //写入日志
        insertSqlLog("删除意见反馈信息 ID：" . $_POST['id']);

        return $db->table('feedbacks')->where($where)->Delete();
    }

    //设置状态
    public function switchStatus()
    {
        $db = $this->db();
        $where['id'] = $_POST['id'];
        $data = [
            'status' => $_POST['status']
        ];
        //写入日志
        insertSqlLog("设置意见反馈显示状态 ID：" . $_POST['id']);

        return $db->table('feedbacks')->where($where)->Update($data);
    }

    //获取数据并分页
    public function jsonPageSelect()
    {
        $db = $this->db();
        $p = intval($_GET['page'] ?? 0) ?: 1;
        $num = intval($_GET['limit'] ?? 0) ?: 15;

        $page = ['num' => $num, 'p' => $p, 'return' => true];
        $result['data'] = $db->table('feedbacks')->Page($page)->order('id DESC')->select();
        $result['page'] = $db->getPage();
        return $result;
    }
}
