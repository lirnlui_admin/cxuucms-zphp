<?php

namespace model;

class visits
{
    //获取列表
    static public function selectData()
    {
        $pdo = \z\pdo::Init();
        $prefix = $pdo->GetConfig();
        $fix = $prefix['prefix'];
        $Sql = "SELECT * FROM {$fix}visits order by id desc limit 30";
        $data = $pdo->QueryAll($Sql);
        return array_reverse($data);
    }
}
