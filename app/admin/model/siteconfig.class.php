<?php

namespace model;

use root\base\model;

class siteconfig extends model
{
	//获取数据
	public function FindData()
	{
		$db = $this->db();
		$where['name'] = 'siteinfo'; //默认获取网站设置
		if (isset($_GET['name'])) 
			$where['name'] = $_GET['name'];
		$result = $db->table('siteconfig')->where($where)->find();
		return unserialize($result['data']);
	}


	//更新数据
	public function updateData($name)
	{
		$db = $this->db();
		if ($name == 'siteinfo') {
			$data = [
				'sitename' => varFilter($_POST['sitename']),
				'siteurl' => varFilter($_POST['siteurl']),
				'keywords' => varFilter($_POST['keywords']),
				'descript' => varFilter($_POST['descript']),
				'copyright' => varFilter($_POST['copyright']),
			];
		} else if ($name == 'cache') {
			$data = [
				'indexhtml' => varFilter($_POST['indexhtml']),
				'indexhtmltime' => varFilter($_POST['indexhtmltime']),
				'visitscache' => varFilter($_POST['visitscache']),
				'visitsnum' => varFilter($_POST['visitsnum']),
				'visitstime' => varFilter($_POST['visitstime']),
				'visitsshowtime' => varFilter($_POST['visitsshowtime']),
				'publishtime' => varFilter($_POST['publishtime']),
				'publishyear' => varFilter($_POST['publishyear']),
			];
		} else if ($name == 'upload') {
			$data = [
				'imguploadsize' => varFilter($_POST['imguploadsize']),
				'attuploadsize' => varFilter($_POST['attuploadsize']),
				'imguploadext' => varFilter($_POST['imguploadext']),
				'attuploadext' => varFilter($_POST['attuploadext']),
				'thumbON' => varFilter($_POST['thumbON']),
				'maxWidth' => varFilter($_POST['maxWidth']),
				'maxHeight' => varFilter($_POST['maxHeight']),
				'waterON' => varFilter($_POST['waterON']),
				'waterImg' => varFilter($_POST['waterImg']),
				'waterPos' => varFilter($_POST['waterPos']),
			];
		}

		$where['name'] = $name;
		$ser_data['data'] = serialize($data);

		$re_key = $db->table('siteconfig')->Where($where)->Update($ser_data);
		if ($re_key) {
			//写入日志
			insertSqlLog("修改系统配置：" . $name);

			//更新缓存
			$fix = $db->GetPrefix();
			$db->CleanCache($GLOBALS['ZPHP_CONFIG']['DB']['db'], $fix . "siteconfig");

			return ['status' => 1, 'msg' => '修改成功'];
		} else {
			return ['status' => 0, 'msg' => '数据没有变化！'];
		}
	}

	
	//查找对应的网站设置信息
	// $val string  如： 系统设置：siteinfo  缓存设置：cache  上传设置：upload
	public static function getConfig(string $val)
	{
		$db = \ext\db::Init();
		$where['name'] = $val;
		$result = $db->table('siteconfig')->where($where)->cache(833600)->find();
		return unserialize($result['data']);
	}


	//查找对应的网站上传设置信息
	// $val string  如： 附件类型：attuploadext  图片类型：imguploadext   图片上传大小限制：imguploadsize 附件上传大小限制：attuploadsize
	public static function getUpload()
	{
		$value = self::getConfig('upload');
		$result['imguploadsize'] = $value['imguploadsize'];
		$result['attuploadsize'] = $value['attuploadsize'];
		$result['imguploadext'] = str_replace('.', '',$value['imguploadext']);
		$result['attuploadext'] = str_replace('.', '',$value['attuploadext']);//, exts: 'zip|rar|7z|doc|docx|xlsx|xls|'
		return $result;
	}

	//根据数组键名查找对应的系统设置信息
    public static function getSiteInfo($key)
    {
		$result = self::getConfig('siteinfo');
        return $result[$key];
	}

	//根据数组键名查找对应的网站信息
    public static function getCache($key)
    {
		$result = self::getConfig('cache');
        return $result[$key];
	}

}
