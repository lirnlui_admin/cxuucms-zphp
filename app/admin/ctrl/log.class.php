<?php

namespace ctrl;

use model\log_sql;
use z\view;

class log
{
	static function init()
	{
		\common\middleware\check::isLogin();//判断是否登录
		\common\middleware\check::auth(); //判断用户权限
	}

	public static function index()
	{
		if (isset($_GET['list_json'])) {
			$d = new log_sql;
			$result = $d->jsonPageSelect();
			$jsonlist = array();
			$jsonlist['code'] = 0;
			$jsonlist['msg'] = 'ok';
			$jsonlist['count'] = $result['page']['rows'];
			$jsonlist['data'] = $result['data'];
			exit(json_encode($jsonlist));
		} else {
			view::display();
		}
	}
}
