<?php

namespace ctrl;

use model\members;
use z\view;

class member
{
	static function init()
	{
		\common\middleware\check::isLogin();//判断是否登录
		\common\middleware\check::auth(); //判断用户权限
	}

	public static function index()
	{
		if (isset($_GET['list_json'])) {
			$d = new members;
			$result = $d->jsonPageSelect();
			$jsonlist = array();
			$jsonlist['code'] = 0;
			$jsonlist['msg'] = 'ok';
			$jsonlist['count'] = $result['page']['rows'];
			$jsonlist['data'] = $result['data'];
			exit(json_encode($jsonlist));
		} else {
			view::display();
		}
	}

	public static function add()
	{
		if (isset($_GET['view'])) {
			view::display();
		} else {
			$m = new members;
			$result = $m->insertData();
			json(array('status' => $result['status'], 'info' => $result['msg']));
		}
	}

	public static function edit()
	{
		if (isset($_GET['view'])) {
			if (isset($_GET['id'])) {
				$u = new members;
				$info = $u->findData();
				view::assign('member', $info);
				view::display();
			} else {
				return '缺少参数:id';
			}
		} else {
			//验证用户名是否存在
			$m = new members;
			$result = $m->updateData();
			json(array('status' => $result['status'], 'info' => $result['msg']));
		}
	}

	public static function switchStatus()
	{
		if (isset($_POST['id'])) {
			$m = new members;
			$edit = $m->switchStatus();
			if ($edit) {
				json(array('status' => 1, 'info' => '设置成功'));
			}
		} else {
			json(array('status' => 0, 'info' => 'ID错误！'));
		}
	}

	public static function del()
	{
		if (isset($_POST['id'])) {
			$m = new members;
			$del = $m->deleteOneData();
			if ($del) {
				json(array('status' => 1, 'info' => '删除成功'));
			}
		} else {
			json(array('status' => 0, 'info' => 'ID错误！'));
		}
	}
}
