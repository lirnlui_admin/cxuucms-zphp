<?php

namespace ctrl;

use z\view;

class cache
{
	static function init()
	{
		\common\middleware\check::isLogin(); //判断是否登录
		\common\middleware\check::auth(); //判断用户权限
	}

	/*
	缓存管理
	*/
	public static function index()
	{
		$dirsize = array();
		/**前台静态缓存 */
		$dirsize['htmlCahe'] = FileSizeFormat(dirSize(P_HTML));
		$htmlCahe = dirFileNum(P_HTML); //计算文件及目录数量
		$dirsize['fileHtmlCaheNum'] = $htmlCahe['fileNum'] ?? 0;
		$dirsize['dirHtmlCaheNum'] = $htmlCahe['dirNum'] ?? 0;
		/**文件缓存 */
		$dirsize['fileCahe'] = FileSizeFormat(dirSize(P_CACHE));
		$fileCacheNum = dirFileNum(P_CACHE);
		$dirsize['fileCaheNum'] = $fileCacheNum['fileNum'] ?? 0;
		$dirsize['dirCaheNum'] = $fileCacheNum['dirNum'] ?? 0;
		/**前台程序缓存 */
		$dirsize['fileHomeCaheDir'] = P_RUN . 'index/';
		$fileHomeCaheDir = dirFileNum($dirsize['fileHomeCaheDir']); //计算文件及目录数量
		$dirsize['fileHomeCaheNum'] = $fileHomeCaheDir['fileNum'] ?? 0;
		$dirsize['dirHomeCaheNum'] = $fileHomeCaheDir['dirNum'] ?? 0;
		$dirsize['fileHomeCahe'] = FileSizeFormat(dirSize($dirsize['fileHomeCaheDir']));
		/**后台程序缓存 */
		$dirsize['fileAdminCaheDir'] = P_RUN . 'admin/';
		$fileAdminCaheDir = dirFileNum($dirsize['fileAdminCaheDir']); //计算文件及目录数量
		$dirsize['fileAdminCaheNum'] = $fileAdminCaheDir['fileNum'] ?? 0;
		$dirsize['dirAdminCaheNum'] = $fileAdminCaheDir['dirNum'] ?? 0;
		$dirsize['fileAdminCahe'] = FileSizeFormat(dirSize($dirsize['fileAdminCaheDir']));
		/**日志 */
		$dirsize['warningLogDir'] = P_TMP . 'warning_log/';
		$warningLogDir = dirFileNum($dirsize['warningLogDir']); //计算文件及目录数量
		$dirsize['fileWarningLogNum'] = $warningLogDir['fileNum'] ?? 0;
		$dirsize['dirWarningLogNum'] = $warningLogDir['dirNum'] ?? 0;
		$dirsize['warningLog'] = FileSizeFormat(dirSize($dirsize['warningLogDir']));
		view::assign('dirsize', $dirsize);
		view::display();
	}


	//清除文件缓存
	static function delFileCache()
	{
		switch ($_GET['id']) {
			case 1:
				//del_dir(P_CACHE, 1);
				$pdo = \z\pdo::Init();
				$pdo->CleanCache($GLOBALS['ZPHP_CONFIG']['DB']['db']);
				//写入日志
				$log_sql = new \model\log_sql();
				$log_sql->insertData("清除 数据库缓存");

				json(array('status' => 1, 'info' => '清除 数据库缓存 成功'));
				break;
			case 2:
				del_dir(P_RUN . '/admin', 1);
				//写入日志
				$log_sql = new \model\log_sql();
				$log_sql->insertData("清除 后台程序缓存");

				json(array('status' => 1, 'info' => '清除 后台程序缓存 成功'));
				break;
			case 3:
				del_dir(P_HTML, 1);
				//写入日志
				$log_sql = new \model\log_sql();
				$log_sql->insertData("清除 HTML缓存");

				json(array('status' => 1, 'info' => '清除 HTML缓存 成功'));
				break;
			case 4:
				del_dir(P_RUN . '/index', 1);
				//写入日志
				$log_sql = new \model\log_sql();
				$log_sql->insertData("清除 前台程序缓存");

				json(array('status' => 1, 'info' => '清除 前台程序缓存 成功'));
				break;
			case 5:
				del_dir(P_TMP . 'warning_log/', 1);
				//写入日志
				$log_sql = new \model\log_sql();
				$log_sql->insertData("清除 系统日志");

				json(array('status' => 1, 'info' => '清除 系统日志 成功'));
				break;
			case 6:
				del_file("index.html");
				//写入日志
				$log_sql = new \model\log_sql();
				$log_sql->insertData("删除 前台首页 index.html 文件");

				json(array('status' => 1, 'info' => '清除 前台首页 index.html 文件 成功'));
				break;
		}
	}
}
