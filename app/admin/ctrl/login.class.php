<?php

namespace ctrl;

use model\admin_user;
use model\admin_group;
use z\view;

class login
{
	public static function index()
	{
		view::display();
	}

	static function login()
	{
		$m = new admin_user;
		$result = $m->findAdmin();
		if (empty($result)) {
			json(array('status' => 0, 'info' => '用户名或者密码错误'));
		}
		if ($result['status'] == 1) {
			//session_start();
			$find = $result['gid'];
			$d = new admin_group;			
			$groupinfo = $d->findData($find);
			$adminUserInfo = array(
				'userid' => $result['id'],
				'username' => $result['username'],
				'gid' => $result['gid'],
				'nickname' => $result['nickname'],
				'groupname' => $groupinfo['groupname'],
				'systemrole' => $groupinfo['systemrole'],
				'channlrole' => $groupinfo['channlrole'],
				'menurole' => $groupinfo['menurole'],
				'logintime' => $result['logintime'],
				'loginip' => getip(),
			);
			$_SESSION["cxuu_admin"] = $adminUserInfo;
			//setcookie("cxuu_admin", $adminUserInfo, time()+3600);

			$m->loginDbInsert($result['id']); //将登录信息写入数据库

			$log_sql = new \model\log_sql();
			$log_sql->insertData("登录成功"); //写入日志

			json(array('status' => 1, 'info' => '登录成功!'));
		} else {
			json(array('status' => 0, 'info' => '帐户被禁用！'));
		}
	}


	static function loginOut()
	{
		unset($_SESSION["cxuu_admin"]);
		//setcookie("cxuu_admin", "", time()-3600);
		redirect(PHP_FILE);
	}

	//验证码生成函数
	//访问路径 /admin.php?c=login&a=verify
	/* static function verify(){
        $img = new \ext\verimg();
		$img->img();
    } */
}
