<?php

namespace ctrl;

use model\siteconfig;
use z\view;

class system
{
	static function init()
	{
		\common\middleware\check::isLogin();//判断是否登录
		\common\middleware\check::auth(); //判断用户权限
	}
	public static function index()
	{
		$m = new siteconfig;
		$config = $m->FindData();
		view::assign('config', $config);
		if (isset($_GET['name'])) {
			view::display($_GET['name']);
		} else {
			view::display('siteinfo');
		}
	}

	public static function edit()
	{
		$m = new siteconfig;
		$result = $m->updateData($_GET['name']);
		json(array('status' => $result['status'], 'info' => $result['msg']));
	}
}
