<?php

namespace ctrl;

use model\article;
use model\article_cate;
use model\siteconfig;
use z\view;

class content
{
	static function init()
	{
		\common\middleware\check::isLogin(); //判断是否登录
		\common\middleware\check::auth(); //判断用户权限
	}

	public static function index()
	{
		if (isset($_GET['list_json'])) {
			$d = new article;
			$result = $d->jsonPageSelect();
			$jsonlist = array();
			$jsonlist['code'] = 0;
			$jsonlist['msg'] = 'ok';
			$jsonlist['count'] = $result['page']['rows'];
			$jsonlist['data'] = $result['data'];
			exit(json_encode($jsonlist));
		} else {
			if (isset($_GET['cid'])) {
				$cid = $_GET['cid'];
			} else {
				$cid = '';
			}
			$m = new article_cate;
			$cateListTree = $m->SelectTreeData();
			view::assign('catelist', $cateListTree);
			view::assign('cid', $cid);
			view::display();
		}
	}


	public static function add()
	{
		if (isset($_GET['view'])) {
			$m = new article_cate;
			$cateListTree = $m->SelectTreeData();
			//获取上传类型
			$upload = siteconfig::getUpload();
			view::assign('attuploadext', $upload['attuploadext']);
			view::assign('imguploadext', $upload['imguploadext']);
			view::assign('imguploadsize', $upload['imguploadsize']);
			view::assign('attuploadsize', $upload['attuploadsize']);
			view::assign('list', $cateListTree);
			view::display();
		} else {
			$m = new article;
			$result = $m->insertData();
			json(array('status' => $result['status'], 'info' => $result['msg'], 'cid' => $result['cid']));
		}
	}

	public static function edit()
	{
		if (isset($_GET['view'])) {
			//获取当前ID的一条内容信息
			$m = new article;
			$info = $m->findData();
			//获取栏目树形数据
			$catelist = new article_cate;
			$cateListTree = $catelist->SelectTreeData();
			//获取上传类型
			$upload = siteconfig::getUpload();
			view::assign('attuploadext', $upload['attuploadext']);
			view::assign('imguploadext', $upload['imguploadext']);
			view::assign('imguploadsize', $upload['imguploadsize']);
			view::assign('attuploadsize', $upload['attuploadsize']);
			view::assign('list', $cateListTree);
			view::assign('info', $info['content']);
			view::assign('infoatt', $info['attments']);
			view::display();
		} else {
			$m = new article;
			$result = $m->updateData();
			json(array('status' => $result['status'], 'info' => $result['msg'], 'cid' => $result['cid']));
		}
	}

	public static function switchStatus()
	{
		if (isset($_POST['id'])) {
			$m = new article;
			if ($m->switchStatus())
				json(array('status' => 1, 'info' => '设置成功'));
		} else {
			json(array('status' => 0, 'info' => 'ID错误！'));
		}
	}

	public static function del()
	{
		if (isset($_POST['id'])) {
			$m = new article;
			if ($m->deleteOneData()) 
				json(array('status' => 1, 'info' => '删除成功'));
		} else {
			json(array('status' => 0, 'info' => 'ID错误！'));
		}
	}
}
