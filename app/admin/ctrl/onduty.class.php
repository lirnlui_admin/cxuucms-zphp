<?php

namespace ctrl;

use model\ondutys;
use z\view;

class onduty
{
	static function init()
	{
		\common\middleware\check::isLogin();//判断是否登录
		\common\middleware\check::auth(); //判断用户权限
	}

	public static function index()
	{
		if (isset($_GET['list_json'])) {
			$d = new ondutys;
			$result = $d->jsonPageSelect();
			$jsonlist = array();
			$jsonlist['code'] = 0;
			$jsonlist['msg'] = 'ok';
			$jsonlist['count'] = $result['page']['rows'];
			$jsonlist['data'] = $result['data'];
			exit(json_encode($jsonlist));
		} else {
			view::display();
		}
	}

	public static function add()
	{
		if (isset($_GET['view'])) {
			view::display();
		} else {
			$m = new ondutys;
			$result = $m->insertData();
			json(array('status' => $result['status'], 'info' => $result['msg']));
		}
	}


	public static function edit()
	{
		if (isset($_GET['view'])) {
			if (isset($_GET['id'])) {
				$m = new ondutys;
				$onduty = $m->findData();
				view::assign('onduty', $onduty);
				view::display();
			} else {
				return '缺少参数:id';
			}
		} else {
			$m = new ondutys;
			$result = $m->updateData();
			json(array('status' => $result['status'], 'info' => $result['msg']));
		}
	}

	public static function switchStatus()
	{
		if (isset($_POST['id'])) {
			$m = new ondutys;
			$edit = $m->switchStatus();
			if ($edit) {
				json(array('status' => 1, 'info' => '设置成功'));
			}
		} else {
			json(array('status' => 0, 'info' => 'ID错误！'));
		}
	}

	public static function del()
	{
		if (isset($_POST['id'])) {
			$m = new ondutys;
			$del = $m->deleteOneData();
			if ($del) {
				json(array('status' => 1, 'info' => '删除成功'));
			}
		} else {
			json(array('status' => 0, 'info' => 'ID错误！'));
		}
	}
}
