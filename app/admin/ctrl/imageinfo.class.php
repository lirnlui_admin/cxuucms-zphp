<?php

namespace ctrl;

use model\images_image;
use z\view;

class imageinfo
{
	static function init()
	{
		\common\middleware\check::isLogin();//判断是否登录
		\common\middleware\check::auth(); //判断用户权限
	}

	public static function index()
	{
		if (isset($_GET['list_json'])) {
			$d = new images_image;
			$result = $d->jsonPageSelect();
			$jsonlist = array();
			$jsonlist['code'] = 0;
			$jsonlist['msg'] = 'ok';
			$jsonlist['count'] = $result['page']['rows'];
			$jsonlist['data'] = $result['data'];
			exit(json_encode($jsonlist));
		} else {
			$m = new images_image;
			$images = $m->findImagesData();
			view::assign('images', $images);
			view::assign('aid', $_GET['aid']);
			view::display();
		}
	}

	public static function add()
	{
		if (isset($_GET['view'])) {
			view::assign('aid', $_GET['aid']);
			view::display();
		} else {
			$m = new images_image;
			$result = $m->insertData();
			json(array('status' => $result['status'], 'info' => $result['msg']));
		}
	}

	public static function edit()
	{
		if (isset($_GET['view'])) {
			if (isset($_GET['id'])) {
				$m = new images_image;
				$image = $m->findData();
				view::assign('image', $image);
				view::display();
			} else {
				return '缺少参数:id';
			}
		} else {
			$m = new images_image;
			$result = $m->updateData();
			json(array('status' => $result['status'], 'info' => $result['msg']));
		}
	}

	public static function del()
	{
		if (isset($_POST['id'])) {
			$m = new images_image;
			$del = $m->deleteOneData();
			if ($del) {
				json(array('status' => 1, 'info' => '删除成功'));
			}
		} else {
			json(array('status' => 0, 'info' => 'ID错误！'));
		}
	}
}
