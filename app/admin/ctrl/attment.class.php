<?php

namespace ctrl;

use model\attments;
use z\view;

class attment
{
	static function init()
	{
		\common\middleware\check::isLogin();//判断是否登录
		\common\middleware\check::auth(); //判断用户权限
	}

	public static function index()
	{
		if(isset($_GET['list_json'])){
			$d = new attments;
			$result = $d->jsonPageSelect();
			$jsonlist = array();
			$jsonlist['code'] = 0;
			$jsonlist['msg'] = 'ok';
			$jsonlist['count'] = $result['page']['rows'];
			$jsonlist['data'] = $result['data'];
			exit(json_encode($jsonlist));
		}else{
			view::display();
		}
	}

	public static function del()
	{
		if (isset($_GET['attid'])) {
			$m = new attments;
			$del = $m->deleteOneData();
			if ($del) 
				json(array('status' => 1, 'info' => '删除成功'));
				else
				json(array('status' => 0, 'info' => '删除失败'));
		} else {
			json(array('status' => 0, 'info' => 'ID错误！'));
		}
	}
}
