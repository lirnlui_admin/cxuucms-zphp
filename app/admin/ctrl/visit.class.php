<?php

namespace ctrl;

use model\visits;
use z\view;

class visit
{
	static function init()
	{
		\common\middleware\check::isLogin();//判断是否登录
		\common\middleware\check::auth(); //判断用户权限
	}

	public static function index()
	{
		$d = new visits;
		$visits = $d->selectData();
		view::assign('visits', $visits);
		view::display();
	}
}
