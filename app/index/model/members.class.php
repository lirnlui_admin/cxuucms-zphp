<?php
namespace model;

class members
{
	
   //查询数据列表
   public static function selectData()
   {
        $db = \ext\db::Init();
		$where['status'] = 1;
        return $db->table('member')->where($where)->cache(6600)->order('sort asc')->select();
   }
}
