<?php
namespace model;

use root\base\ex_cache;
use model\siteconfig;

class visits
{
	/**
     * 写入访问量信息
     * id
     * date   datetime
     * visits  int
     */
    public static function insertData()
    {
        $swtich = siteconfig::getCache('visitscache');
        if($swtich == 1){
            $time = siteconfig::getCache('visitstime');
            $num = siteconfig::getCache('visitsnum');//可根据网站访问量适当将数据设置得大一些，建议设置成单数

            //如果开启缓存，访问量记数先写入Redis缓存 达到量后再写库
            $getCache = ex_cache::getCache('visits_inc');
            if($getCache){
                $setCache = ex_cache::setCache('visits_inc', $getCache += 1, $time);
            }else{
                $setCache = ex_cache::setCache('visits_inc', 1, $time);
            }

            if($setCache >= $num){
                ex_cache::setCache('visits_inc', 0, $time);
                $pdo = \z\pdo::Init();
                $prefix = $pdo->GetConfig();
                $fix = $prefix['prefix'];
                $date = date('Y-m-d',time());
                $sql = "UPDATE {$fix}visits SET visits = visits+{$num} WHERE DATEDIFF(date,NOW()) = 0";
                $result = $pdo->Submit($sql);
                if(empty($result)){
                    $insertSql = "INSERT INTO `{$fix}visits` (`id`, `date`, `visits`) VALUES (NULL, '{$date}', '{$num}')";
                    $pdo->Submit($insertSql);
                }
            }
        }else{
            $pdo = \z\pdo::Init();
            $prefix = $pdo->GetConfig();
            $fix = $prefix['prefix'];
            $date = date('Y-m-d',time());
            $sql = "UPDATE {$fix}visits SET visits = visits+1 WHERE DATEDIFF(date,NOW()) = 0";
            $result = $pdo->Submit($sql);
            if(empty($result)){
                $insertSql = "INSERT INTO `{$fix}visits` (`id`, `date`, `visits`) VALUES (NULL, '{$date}', 1)";
                $pdo->Submit($insertSql);
            }
        }

        
    }
	
	/**
     * 获取访问量信息 
     * id
     * date   datetime
     * visits  int
     */
    static public function findData()
    {
        $time = siteconfig::getCache('visitsshowtime')??600;
        $pdo = \z\pdo::Init();
		$prefix = $pdo->GetConfig();
		$fix = $prefix['prefix'];
        //查询今天数
        $todaySql = "SELECT * FROM {$fix}visits WHERE DATEDIFF(date,NOW()) = 0";
        $pdo->Cache($time);
        $todayData = $pdo->QueryOne($todaySql);
        $data['today'] = $todayData['visits']??0;
        //查询昨天数
        $yesterdaySql =  "SELECT * FROM {$fix}visits WHERE DATEDIFF(date,NOW()) = -1";
        $pdo->Cache($time);
        $yesterdayData = $pdo->QueryOne($yesterdaySql);
        $data['yesterday'] = $yesterdayData['visits']??0;
        //查询总数
        $sumSql =  "SELECT SUM(visits) FROM {$fix}visits";
        $pdo->Cache($time);
        $sum = $pdo->QueryOne($sumSql);
        //查询最大数
        $maxSql =  "SELECT MAX(visits) FROM {$fix}visits";
        $pdo->Cache($time);
        $max = $pdo->QueryOne($maxSql);
        $data['sum']=$sum['SUM(visits)'];
        $data['max']=$max['MAX(visits)'];
        return $data;
    }

}
