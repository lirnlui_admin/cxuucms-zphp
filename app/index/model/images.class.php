<?php
namespace model;

class images
{
   //查询数据列表
   public static function selectData()
   {
        $db = \ext\db::Init();
		$where['status'] = 1;
        return $db->table('images')->where($where)->limit(6)->cache(6600)->select();
   }
   public static function listData()
    {
        $db = \ext\db::Init();
		$where['status'] = 1;
		$p = ROUTE['query']['p'];
        $num = 20;  //每页显示条数
        $page = ['num' => $num, 'p' => $p, 'return' => ['prev', 'next', 'first', 'last', 'list']];
		$result['data'] = $db->table('images')->page($page)->order('id DESC')->select();
        $result['page'] = $db->getPage();
        return $result;
    }
   
   //查询数据列表
   public static function selectImageData()
   {
        $db = \ext\db::Init();
		$where['aid'] = ROUTE['query']['id'];
        return $db->table('images_image')->where($where)->cache(6600)->select();
   }

}
