<?php

/**
 * 前端综合 数据模型  
 * 龙啸轩内容管理系统
 **/

namespace model;

class common
{

	//查询用户组的ID 和 用户组名列表，此数据长期缓存
	protected static function getAdminGroup()
	{
		$db = \ext\db::Init();
		return $db->table('admin_group')->Field('id, groupname')->cache(86400)->select();
	}

	//查询用户组的ID 和 用户组名列表，此数据长期缓存
	protected static function getAdminUser()
	{
		$db = \ext\db::Init();
		return $db->table('admin_user')->Field('id, nickname')->Cache(88600)->select();
	}

	//查询所有栏目列表
	protected static function getCateData()
	{
		$db = \ext\db::Init();
		return $db->table('article_cate')->Cache(0)->Order('sort ASC')->Select();
	}

	//查询指定栏目列表
	public static function getCateList($id = null, $cache = 0, $order = 'sort ASC')
	{
		$db = \ext\db::Init();
		if (!empty($id)) {
			$where = "id IN (" . $id . ")";
			return $db->table('article_cate')->where($where)->Cache($cache)->Order($order)->Select();
		} else {
			return self::getCateData();
		}
	}

	//文章内容栏目路径显示，前端根据数据进行判断，此数据长期缓存
	public static function getCateUrlData($cid)
	{
		$cate = new \common\extend\Category(array('id', 'pid', 'name', 'cname'));
		return sort($cate->getPath(self::getCateData(), $cid)); // 以升序对数组排序
	}

	//根据已经查询到的数组，查找指定的一条栏目数据
	public static function findCateData($cid)
	{
		if (is_numeric($cid))
			$find = filter_by_value(self::getCateData(), 'id', $cid);
		else
			$find = filter_by_value(self::getCateData(), 'urlname', $cid);
		$resName = array_column($find, 'name');
		$resTheme = array_column($find, 'theme');
		$resCid = array_column($find, 'id');
		$resNum = array_column($find, 'num');
		$resurlname = array_column($find, 'urlname');
		$result['name'] = $resName[0];
		$result['theme'] = $resTheme[0];
		$result['id'] = $resCid[0];
		$result['num'] = $resNum[0];
		$result['urlname'] = $resurlname[0];
		return $result;
	}

	//根据已经查询到的数组，查找指定的一条用户组数据
	public static function findAdminGroupData($gid)
	{
		$find = filter_by_value(self::getAdminGroup(), 'id', $gid);
		$result['groupname'] = array_column($find, 'groupname');
		return $result['groupname'][0];
	}

	//根据已经查询到的数组，查找指定的一条用户数据
	public static function findAdminUserData($uid)
	{
		$find = filter_by_value(self::getAdminUser(), 'id', $uid);
		$result['nickname'] = array_column($find, 'nickname');
		return $result['nickname'][0];
	}

	//查询指定数据表的列表
	public static function get_List($data)
	{
		$table = $data['table'];
		switch ($table) {
			case 'article':
				return \model\article::selectData($data['cid'] ?? 0, $data['limit'] ?? 10, $data['cache'] ?? 600, $data['imgbl'] ?? 0, $data['attribute'] ?? 0, $data['order'] ?? "id desc");
				break;
			case 'article_cate':
				return self::getCateList($data['id'] ?? 0, $data['cache'] ?? 0, $data['order'] ?? "sort ASC");
				break;
			case 'images':
				return \model\images::selectData();
				break;
			case 'onduty':
				return \model\ondutys::findData();
				break;
			default:
				//通用列表调用方法  ： cxuuList("table='XXX' key='value' limit='10' cache='600' order='sort DESC'")
				$db = \ext\db::Init();
				if (!empty($data['key']))
					$where[$data['key']] = $data['value'];
				else
					$where = '';
				return $db->table($table)->where($where)->limit($data['limit'] ?? 10)->cache($data['cache'] ?? 0)->order($data['order'] ?? '')->select();
		}
	}
}
