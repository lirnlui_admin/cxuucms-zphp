<?php

namespace model;

class article
{
	//关联查找一条数据，个人觉得多个表关联查询会影响效率，改为控制器读取较固定的表缓存实现
	public static function findData()
	{
		$db = \ext\db::Init();
		$where['a.id'] = ROUTE['query']['id'];
		$join = 'LEFT JOIN article_content b ON a.id=b.aid AND a.status = 1';
		$result['content'] = $db->table('article a')->Join($join)->where($where)->find();
		if (!empty($result['content'])) {
			if (!empty($result['content']['attid'])) {
				//获取附件列表
				$wheresAtt = "attid IN (" . $result['content']['attid'] . ")";
				$result['attments'] = $db->table('attments')->where($wheresAtt)->Select();
			}
			return $result;
		} else {
			return false;
		}
	}
	//更新浏览次数据  +1
	public static function hitsData()
	{
		$db = \ext\db::Init();
		$where['id'] = ROUTE['query']['id'];
		$save = ['hits' => '{{hits + 1}}']; // hits字段值增加1
		return $db->table('article')->where($where)->Update($save);
	}

	//查询 GET值 栏目分页列表  支持分页缓存->cache(600)
	public static function listData($cateVal)
	{
		$result['cate'] = \model\common::findCateData($cateVal); //查询所在栏目数据
		$where['cid'] = $result['cate']['id'];
		$where['status'] = 1;
		$p = ROUTE['query']['p'];
		$num = $result['cate']['num'] ?? 25;  //每页显示条数
		$page = ['num' => $num, 'p' => $p, 'return' => ['prev', 'next', 'first', 'last', 'list']];
		$db = \ext\db::Init();
		$result['data'] = $db->table('article')->where($where)->page($page)->order('id DESC')->select();
		$result['page'] = $db->getPage();
		return $result;
	}

	/**
	 *  根据条件 联表 查询指定内容列表  并查出当前所属栏目信息
	 *	$cid = 栏目ID   $limit  = 1,10条数,  $cache  = 缓存时间 秒
	 *	使用方法: 在模板位置  循环 foreach(\model\article::selectJoinData(7,5,60) as $vo){ echo $vo['catename']; }
	 **/
	public static function selectJoinData($cid, $limit, $cache)
	{
		$db = \ext\db::Init();
		$where['a.cid'] = $cid;
		$where['a.status'] = 1;
		$field = 'b.id as cateid , b.name as catename ,a.id,a.cid,a.title,a.status,a.time';
		$join = 'LEFT JOIN article_cate b ON a.cid=b.id';
		return $db->table('article a')->field($field)->Join($join)->cache($cache)->where($where)->limit($limit)->select();
	}


	/**
	 *  根据条件 查询指定内容列表
	 *	$cid = 栏目ID
	 *	$limit  = 1,10条数,
	 *	$cache  = 缓存时间 秒
	 *	使用方法: 在模板位置  循环 foreach(\model\article::selectData(7,5,60) as $vo){ echo $vo['title']; }
	 **/
	public static function selectData($cid = 0, $limit = 10, $cache = 600, $imgbl = 0, $attribute = 0, $order = "id DESC")
	{
		$db = \ext\db::Init();
		if ($cid == 0) $where = "status = 1";
		else $where = "cid IN (" . $cid . ") AND status = 1";
		if ($imgbl == 1) $where .= " AND imgbl = 1";
		if ($attribute == 1) $where .= " AND attribute_a = 1";
		if ($attribute == 2) $where .= " AND attribute_b = 1";
		if ($attribute == 3) $where .= " AND attribute_c = 1";
		return $db->table('article')->cache($cache)->where($where)->limit($limit)->order($order)->select();
	}


	//信息统计查询
	public static function findCount()
	{
		$data = [];
		$pdo = \z\pdo::Init();
		$prefix = $pdo->GetConfig();
		$fix = $prefix['prefix'];

		$cachetime = siteconfig::getCache('publishtime')??1 * 86400;

		if (siteconfig::getCache('publishyear') == 1)
			//所有数据统计
			$data['year'] = $pdo->Cache($cachetime)->QueryAll("SELECT COUNT(1) AS total, a.gid,b.groupname FROM {$fix}article a inner JOIN {$fix}admin_group b ON b.id=a.gid GROUP BY gid ORDER BY total desc");//WHERE b.id !=1
		else 
			//统计当年
			$data['year'] = $pdo->Cache($cachetime)->QueryAll("SELECT COUNT(1) AS total, a.gid,b.groupname FROM {$fix}article a inner JOIN {$fix}admin_group b ON b.id=a.gid AND DATE_FORMAT(a.time,'%Y') = DATE_FORMAT(NOW(),'%Y') GROUP BY gid ORDER BY total desc");
		

		//统计本月
		//$data['month'] = $pdo->Cache($cachetime)->QueryAll("SELECT COUNT(1) AS total, a.gid,b.groupname FROM {$fix}article a inner JOIN {$fix}admin_group b ON b.id=a.gid WHERE DATE_FORMAT(a.time,'%Y-%m') = DATE_FORMAT(NOW(),'%Y-%m') GROUP BY gid asc");

		//指定年统计
		//$bind = [':yeare' => 2021]; //绑定参数
		//$data['whatyear'] = $pdo->Cache($cachetime)->QueryAll("SELECT COUNT(1) AS total, a.gid,b.groupname FROM {$fix}article a inner JOIN {$fix}admin_group b ON b.id=a.gid WHERE DATE_FORMAT(a.time,'%Y') = DATE_FORMAT(:yeare,'%Y') GROUP BY gid ORDER BY total desc", $bind);


		return $data;
	}
}
