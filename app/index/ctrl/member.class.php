<?php

namespace ctrl;

use model\members;
use z\view;

class member
{
	static function init()
	{
		\model\visits::insertData(); //用户访问记录 写入记数
	}

	public static function index()
	{
		/* if(!isset($_GET['id']) & empty($_GET['id']) & empty(ROUTE['params']['id'])){
			return  parent::_404();
		} */

		$list = members::selectData();
		view::assign('list', $list);
		view::Display();
	}
}
