<?php

namespace ctrl;

use model\images;
use z\view;

class imagelist
{
	static function init()
	{
		\model\visits::insertData(); //用户访问记录 写入记数
	}

	public static function index()
	{
		$list = images::listData();
		view::assign('list', $list['data']);
		view::assign('page', $list['page']);
		view::Display();
	}
}
