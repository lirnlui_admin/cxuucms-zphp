<?php
namespace ctrl;

use root\base\ctrl;
use model\article;
use model\common;
use z\view;

class content extends ctrl
{
	static function init(){
		\model\visits::insertData();//用户访问记录 写入记数
	}
	
    public static function index()
    {
 		if(!isset($_GET['id']) & empty($_GET['id']) & empty(ROUTE['params']['id']))
			return  parent::_404();
		
		$info = article::findData();
		if(!$info)
			return  parent::_404();

		$info['content']['groupname'] = common::findAdminGroupData($info['content']['gid']);
		$info['content']['nickname'] = common::findAdminUserData($info['content']['uid']);

		article::hitsData(); //浏览+1
		
		view::assign('info',$info['content']);
		view::assign('attments',$info['attments']);
		view::assign('cid',$info['content']['cid']);//用于判断栏目路径
		view::Display();
    }


	//调用用户组的发布条数数据并JSON输出
	public static function articlecount(){
		$res = article::findCount();
		$result['data'] = $res['year'];
		$result['code'] = 0;
		$result['msg'] = 'ok';
		exit(json_encode($result));
    }
}
